public class Application{
	public static void main(String[]args){
		//Student type
		Student wu = new Student();
		wu.num = 2242923;
		wu.spec = "human";
		wu.grade = 12;
		
		Student veigar = new Student();
		veigar.num = 2237284;
		veigar.spec = "yordle";
		veigar.grade = 7;
		
		Student[]section3 = new Student[3];
		
		section3[0] = wu;
		section3[1] = veigar;
		section3[2] = new Student();
		section3[2].spec = "undead";
		
		System.out.println(section3[2].spec);
		
		System.out.println(wu.num+" "+wu.spec+" "+wu.grade);
		System.out.println(veigar.num+" "+veigar.spec+" "+veigar.grade);
		
		wu.graduatesIn();
		veigar.graduatesIn();
	}
}